/* This component is the third component and 
is used to demonstrate how multiple components
can be rendered in App */

import React, {Component} from 'react';

class ThirdComponent extends Component {

  render() {
 return (
    <div>
      <p>From component 3</p>
      {this.props.fromApp}
    </div>

    );
  }
}

export default ThirdComponent;
