/* This Component is the first component and 
is used to demonstrate how multiple components
can be rendered in App */


import React, {Component} from 'react';

class FirstComponent extends Component {

  render() {
 return (
    <div>
      <p>From component 1</p>
      {this.props.fromApp}
    </div>

    );
  }
}

export default FirstComponent;
