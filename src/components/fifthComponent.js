/* This component is the fifth component and
is used to demonstrate how to get data in the
fourth components state */

import React, {Component} from 'react';
import FourthComponent from './fourthComponent';

class FifthComponent extends Component {

  render() {
console.log("fifthComponent")
console.log(this.props)
console.log("fifthComponent ***END***")
 return (
    <div>
      <p>From component 5</p>
      <p> [comp 5] -> [comp 4] -> [App] </p>
      <p> [continer for data] -> [has Data] -> [App] </p>
    </div>

    );
  }
}

export default FifthComponent;
