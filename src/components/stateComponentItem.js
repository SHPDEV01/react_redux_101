import React, {Component} from 'react';

class StateComponentItem extends Component {

  render() {
 return (
    <li className="Data">
    <strong>{this.props.data.title} - {this.props.data.content} </strong>
    </li>

    );
  }
}

export default StateComponentItem;
