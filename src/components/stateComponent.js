/* This component is the the state component and
is used to demonstrate how to get the data from the App state*/

import React, {Component} from 'react';
import StateComponentItem from './stateComponentItem';

class StateComponent extends Component {

  render() {
// iterating thru data that is passed from App
console.log("stateComponent")
    let StateComponentItems;
    if (this.props.data) {
      StateComponentItems = this.props.data.map(data => {
//this console.log has to be inside the if statment to log and display each item
      console.log(data)
return (
  <StateComponentItem key={data.title} data={data} />
);
      });
    }

// this console is logging out the data from the state in App

 console.log(this.props)
 console.log("stateComponent ***END***")

 return (
    <div>
      <p>From state component</p>
      {this.props.fromApp}
      <p>***VVVV*** passing data, iterating thru it, and displaying it via stateComponentItems ***VVV***</p>
      {StateComponentItems}

    </div>

    );
  }
}

export default StateComponent;
