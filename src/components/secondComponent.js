/* This component is the second component and
is used to demonstrate how multiple components
can be rendered in App */

import React, {Component} from 'react';

class SecondComponent extends Component {

  render() {
 return (
    <div>
      <p>From component 2</p>
      {this.props.fromApp}
    </div>

    );
  }
}

export default SecondComponent;
