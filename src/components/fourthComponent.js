/* This component is the fourth component and
is used to demonstrate how to pass data from
its state to the fifth component */

import React, {Component} from 'react';
import FifthComponent from './fifthComponent';

class FourthComponent extends Component {
constructor() {
  super()

  this.state = {
   data: [
     {
       component: "4",
       title: "title 1",
       content: "content 1"
     },
     {
       component: "4",
       title: "title 2",
       content: "content 2"
     },
     {
       component: "4",
       title: "title 3",
       content: "content 3"
     }

    ]
  }
}

  render() {
 return (
    <div>
      <p>From component 4</p>
      <FifthComponent data={this.state.data}/>
    </div>

    );
  }
}

export default FourthComponent;
