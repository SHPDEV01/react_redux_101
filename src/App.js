import React, { Component } from 'react';
import './App.css';
import FirstComponent from './components/firstComponent';
import SecondComponent from './components/secondComponent';
import ThirdComponent from './components/thirdComponent';
import FourthComponent from './components/fourthComponent';
import FifthComponent from './components/fifthComponent';
import StateComponent from './components/stateComponent';

const AppText = "This text is from App ";

class App extends Component {

constructor(props) {
  super(props);
  this.state = {

    data: []
  }
}

//proper way to store data
//https://www.youtube.com/watch?v=A71aqufiNtQ TIME 23:34 - 25:16
componentWillMount() {
this.setState( {
  data: [
  {
    title: "title 1",
    content: "content 1"
  },
  {
    title: "title 2",
    content: "content 2"

  },
  {
    title: "title 3",
    content: "content 3"

      }
  ]});
}

  render() {
    return (
      <div className="App">
      <p>From App</p>

      <div className="FirstComponent">
        <FirstComponent  fromApp={AppText + "to firstComponent"} />
      </div>

      <div className="SecondComponent">
        <SecondComponent fromApp={AppText  + "to secondComponent"}/>
      </div>

      <div className="ThirdComponent">
        <ThirdComponent fromApp= {AppText  + "to thirdComponent"}/>
      </div>

      <div className="FourthComponent">
        <FourthComponent fromApp= {AppText  + "to FifthComponent"}/>
      </div>

      <div className="StateComponent">
        <StateComponent
         fromApp = {AppText  + "to stateComponent"}
         data={this.state.data}
        />
      </div>

      </div>
    );
  }
}

export default App;
